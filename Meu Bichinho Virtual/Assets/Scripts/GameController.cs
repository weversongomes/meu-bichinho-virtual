﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController instance;
    public Animal animal;
    public GameObject feedButton;
    public GameObject healthbar;
    public GameObject energybar;

    // Use this for initialization
    void Awake () {
		if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        
		/*if (feedButton) {

        }*/
        //GetComponent<Animator>().SetTrigger("Happy");
    }

    public void Food() {
        animal.Jump();
    }

    public void Exercise()
    {
        animal.Jump();
        SetHealth(25);
        SetEnergy(75);
    }

    public void Play()
    {
        animal.Walk();
    }

    public void SetHealth(int value)
    {
        if (value == 25)
        {
            healthbar.transform.localScale = new Vector2(0.25f, healthbar.transform.localScale.y);
            healthbar.transform.position = new Vector2(-10.85f, 4.12f);
        }
        else if (value == 50)
        {
            healthbar.transform.localScale = new Vector2(0.5f, healthbar.transform.localScale.y);
            healthbar.transform.position = new Vector2(-10.43f, 4.12f);
        }
        else if (value == 75)
        {
            healthbar.transform.localScale = new Vector2(0.75f, healthbar.transform.localScale.y);
            healthbar.transform.position = new Vector2(-10.01f, 4.12f);
        }
        else
        {
            healthbar.transform.localScale = new Vector2(1f, healthbar.transform.localScale.y);
            healthbar.transform.position = new Vector2(-9.6f, 4.12f);
        }
    }

    public void SetEnergy(int value)
    {
        if (value == 25)
        {
            energybar.transform.localScale = new Vector2(0.25f, energybar.transform.localScale.y);
            energybar.transform.position = new Vector2(-10.85f, 2.84f);
        }
        else if (value == 50)
        {
            energybar.transform.localScale = new Vector2(0.5f, energybar.transform.localScale.y);
            energybar.transform.position = new Vector2(-10.43f, 2.84f);
        }
        else if (value == 75)
        {
            energybar.transform.localScale = new Vector2(0.75f, energybar.transform.localScale.y);
            energybar.transform.position = new Vector2(-10.01f, 2.84f);
        }
        else
        {
            energybar.transform.localScale = new Vector2(1f, energybar.transform.localScale.y);
            energybar.transform.position = new Vector2(-9.6f, 2.84f);
        }
    }
}
