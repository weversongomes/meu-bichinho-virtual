﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour {
    public Animator anim;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		/*if (Input.GetMouseButtonDown(0)) {
            anim.SetTrigger("Happy");
        }*/
    }

    public void Jump()
    {
        anim.SetTrigger("jump");
    }

    public void Exercise()
    {
        anim.SetTrigger("jump");
    }

    public void Walk()
    {
        anim.SetTrigger("Walk");
    }

    void OnMouseDown()
    {
        //Debug.Log("Sprite Clicked");
    }
}
